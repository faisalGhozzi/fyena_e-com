const express = require('express')
const router = express.Router()


// pulling functions from controllers/auth.js
const {register, login, forgotpassword, resetpassword} = require('../controllers/auth');


// Building routes for every function
// router.route("/register").post(register)
router.post("/register", register);

router.post("/login", login);

router.post("/forgotpassword", forgotpassword);

router.put("/resetpassword/:resetToken", resetpassword);

module.exports = router;