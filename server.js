require('dotenv').config({path: "./config.env"})
const express = require('express')
const connectDB = require('./config/db');
const errorHandler = require('./middleware/error')

// Connect DB
connectDB();

const app = express();

// After setting up routes/auth.js
app.use(express.json());

app.use('/api/auth', require('./routes/auth'))

// Error handler (last piece of middleware)
app.use(errorHandler)

const PORT = process.env.PORT || 5000

const server = app.listen(PORT, () => {
    console.log(`App listening on port ${PORT}`)
})

process.on("unhandledRejection", (err, promise) => {
    console.log(`Logged Error ${err}`);
    server.close(() => process.exit(1))
})